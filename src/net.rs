use crate::{
    paths::{EmitterNode, Paths, ResponderNode},
    utilities::ReusableList,
};

use std::collections::BTreeSet as Set;

#[derive(Clone)]
pub struct EventNode {
    pub responders: Set<usize>,
    pub emitters: Set<usize>,
}

impl EventNode {
    fn new() -> Self {
        Self {
            responders: Set::new(),
            emitters: Set::new(),
        }
    }
}

#[derive(Default)]
pub struct Net {
    events: ReusableList<EventNode>,
    paths: Paths,
}

impl Net {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn events(&self) -> &[EventNode] {
        &self.events[..]
    }

    pub fn responders(&self) -> &[ResponderNode] {
        self.paths.responders()
    }

    pub fn emitters(&self) -> &[EmitterNode] {
        self.paths.emitters()
    }

    pub fn is_responder_of(&self, responder: usize, event: usize) -> bool {
        self.paths.is_responder_of(responder, event)
    }

    pub fn is_emitter_of(&self, emitter: usize, event: usize) -> bool {
        self.paths.is_emitter_of(emitter, event)
    }

    pub fn add_event(&mut self) -> usize {
        self.events.add(EventNode::new())
    }

    pub fn remove_event(&mut self, event: usize) {
        let event_node = std::mem::replace(&mut self.events[event], EventNode::new());
        for &responder in &event_node.responders {
            self.remove_responder(responder);
        }
        for &emitter in &event_node.emitters {
            self.remove_emitter(emitter);
        }
        self.events.remove(event);
    }

    pub fn add_responder(&mut self, event: usize) -> usize {
        let responder = self.paths.add_responder(event);
        self.events[event].responders.insert(responder);
        responder
    }

    pub fn duplicate_responder(&mut self, responder: usize) -> usize {
        let responder_node = &self.responders()[responder];
        let event = responder_node.event;
        let responder_duplicate = self.paths.duplicate_responder(responder);
        self.events[event].responders.insert(responder_duplicate);
        responder_duplicate
    }

    pub fn remove_responder(&mut self, responder: usize) {
        let responder_node = &self.responders()[responder];
        let event = responder_node.event;
        self.events[event].responders.remove(&responder);
        self.paths.remove_responder(responder);
    }

    pub fn add_emitter(&mut self, event: usize) -> usize {
        let emitter = self.paths.add_emitter(event);
        self.events[event].emitters.insert(emitter);
        emitter
    }

    pub fn duplicate_emitter(&mut self, emitter: usize) -> usize {
        let emitter_node = &self.emitters()[emitter];
        let event = emitter_node.event;
        let emitter_duplicate = self.paths.duplicate_emitter(emitter);
        self.events[event].emitters.insert(emitter_duplicate);
        emitter_duplicate
    }

    pub fn remove_emitter(&mut self, emitter: usize) {
        let emitter_node = &self.emitters()[emitter];
        let event = emitter_node.event;
        self.events[event].emitters.remove(&emitter);
        self.paths.remove_emitter(emitter);
    }

    pub fn connect(&mut self, emitter: usize, responder: usize) {
        self.paths.connect(emitter, responder);
    }

    pub fn disconnect(&mut self, emitter: usize, responder: usize) {
        self.paths.disconnect(emitter, responder);
    }

    pub fn is_connected(&self, emitter: usize, responder: usize) -> bool {
        self.paths.is_connected(emitter, responder)
    }
}
