use crate::net::Net;

use data_stream::{FromStream, ToStream, collections::SizeSettings, from_stream, to_stream};

use std::{
    collections::BTreeSet as Set,
    fmt,
    io::{Read, Result as StreamingResult, Write},
};

struct PathDefinition {
    activated: usize,
    dependencies: Vec<usize>,
}

impl<S: SizeSettings> ToStream<S> for PathDefinition {
    fn to_stream<W: Write>(&self, stream: &mut W) -> StreamingResult<()> {
        to_stream::<S, _, _>(&self.activated, stream)?;
        to_stream::<S, _, _>(&self.dependencies, stream)
    }
}

impl<S: SizeSettings> FromStream<S> for PathDefinition {
    fn from_stream<R: Read>(stream: &mut R) -> StreamingResult<Self> {
        Ok(Self {
            activated: from_stream::<S, _, _>(stream)?,
            dependencies: from_stream::<S, _, _>(stream)?,
        })
    }
}

struct ParallelDefinition {
    dependency: usize,
    parallels: Vec<usize>,
}

impl<S: SizeSettings> ToStream<S> for ParallelDefinition {
    fn to_stream<W: Write>(&self, stream: &mut W) -> StreamingResult<()> {
        to_stream::<S, _, _>(&self.dependency, stream)?;
        to_stream::<S, _, _>(&self.parallels, stream)
    }
}

impl<S: SizeSettings> FromStream<S> for ParallelDefinition {
    fn from_stream<R: Read>(stream: &mut R) -> StreamingResult<Self> {
        Ok(Self {
            dependency: from_stream::<S, _, _>(stream)?,
            parallels: from_stream::<S, _, _>(stream)?,
        })
    }
}

pub struct NetDefinition {
    events: usize,
    paths: Vec<PathDefinition>,
    parallels: Vec<ParallelDefinition>,
}

impl<S: SizeSettings> ToStream<S> for NetDefinition {
    fn to_stream<W: Write>(&self, stream: &mut W) -> StreamingResult<()> {
        to_stream::<S, _, _>(&self.events, stream)?;
        to_stream::<S, _, _>(&self.paths, stream)?;
        to_stream::<S, _, _>(&self.parallels, stream)
    }
}

impl<S: SizeSettings> FromStream<S> for NetDefinition {
    fn from_stream<R: Read>(stream: &mut R) -> StreamingResult<Self> {
        Ok(Self {
            events: from_stream::<S, _, _>(stream)?,
            paths: from_stream::<S, _, _>(stream)?,
            parallels: from_stream::<S, _, _>(stream)?,
        })
    }
}

use std::{collections::HashMap, io::BufRead};

#[derive(Copy, Clone, Debug)]
pub enum ParseErrorKind {
    MultiplePaths,
    MultipleParallels,
    InvalidName,
}

impl ParseErrorKind {
    fn error(self, line: usize) -> ParseError {
        ParseError { line, kind: self }
    }
}

#[derive(Debug)]
pub struct ParseError {
    line: usize,
    kind: ParseErrorKind,
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.kind {
            ParseErrorKind::MultiplePaths => {
                write!(f, "Multiple paths found on line {}", self.line)
            }
            ParseErrorKind::MultipleParallels => {
                write!(f, "Multiple parallels found on line {}", self.line)
            }
            ParseErrorKind::InvalidName => write!(f, "Invalid name on line {}", self.line),
        }
    }
}

impl std::error::Error for ParseError {}

impl NetDefinition {
    pub fn to_lines<W: Write>(&self, texts: &[String], w: &mut W) {
        let get_name = |index: usize| {
            if let Some(text) = texts.get(index) {
                text.to_string()
            } else {
                format!("#{index}")
            }
        };
        let get_parallels = |indices: &[usize]| {
            let mut indices = indices.iter();
            let Some(&first) = indices.next() else {
                return String::new();
            };

            let mut result = get_name(first);
            for &index in indices {
                result = format!("{}&{}", result, get_name(index))
            }
            result
        };

        for path in &self.paths {
            let PathDefinition {
                activated,
                dependencies,
            } = path;

            let _ = writeln!(
                w,
                "{}: {}",
                get_name(*activated),
                get_parallels(dependencies),
            );
        }

        for parallel in &self.parallels {
            let ParallelDefinition {
                dependency,
                parallels,
            } = parallel;

            let _ = writeln!(w, "{}^{}", get_name(*dependency), get_parallels(parallels));
        }
    }

    pub fn from_lines<R: BufRead>(reader: &mut R) -> Result<(Self, Vec<String>), ParseError> {
        let mut name_events = HashMap::new();
        let mut names = Vec::new();
        let mut paths = Vec::new();
        let mut parallels = Vec::new();

        let mut get_event = |name: &str, line_index| {
            let name = name.trim();
            if !name.contains(|c: char| c.is_alphanumeric() || c == ' ' || c == '-' || c == '_') {
                return Err(ParseErrorKind::InvalidName.error(line_index));
            }

            Ok(if let Some(&event) = name_events.get(name) {
                event
            } else {
                let event = names.len();
                name_events.insert(name.to_string(), event);
                names.push(name.to_string());
                event
            })
        };

        for (line_index, line) in reader.lines().enumerate() {
            let Ok(line) = line else {
                continue;
            };

            let mut path = line.split(':');
            if let (Some(activated), Some(dependency_list)) = (path.next(), path.next()) {
                if path.next().is_some() {
                    return Err(ParseErrorKind::MultiplePaths.error(line_index));
                }

                let activated_event = get_event(activated, line_index)?;

                let dependency_split = dependency_list.split('&');
                let mut dependency_events = Vec::new();
                for dependency in dependency_split {
                    dependency_events.push(get_event(dependency, line_index)?)
                }

                paths.push(PathDefinition {
                    activated: activated_event,
                    dependencies: dependency_events,
                });

                continue;
            }

            let mut parallel = line.split('^');
            if let (Some(dependency), Some(parallel_list)) = (parallel.next(), parallel.next()) {
                if parallel.next().is_some() {
                    return Err(ParseErrorKind::MultipleParallels.error(line_index));
                }

                let dependency_event = get_event(dependency, line_index)?;

                let parallel_split = parallel_list.split('&');
                let mut parallel_events = Vec::new();
                for parallel in parallel_split {
                    parallel_events.push(get_event(parallel, line_index)?)
                }

                parallels.push(ParallelDefinition {
                    dependency: dependency_event,
                    parallels: parallel_events,
                });

                continue;
            }
        }

        Ok((
            Self {
                events: names.len(),
                paths,
                parallels,
            },
            names,
        ))
    }
}

impl Net {
    pub fn from_definition(definition: &NetDefinition) -> Self {
        let mut result = Self::new();

        for _ in 0..definition.events {
            result.add_event();
        }

        for path in &definition.paths {
            result.add_path(path.activated, &path.dependencies);
        }
        for path in &definition.parallels {
            result.make_parallel(path.dependency, &path.parallels);
        }
        result
    }

    pub fn to_definition(&self) -> NetDefinition {
        let mut paths = Vec::new();
        let mut parallels = Vec::new();

        for (event, event_node) in self.events().iter().enumerate() {
            for &responder in &event_node.responders {
                let mut events = Vec::new();
                let responder_node = &self.responders()[responder];
                for &emitter in &responder_node.emitters {
                    let emitter_node = &self.emitters()[emitter];
                    events.push(emitter_node.event);
                }

                if events.is_empty() {
                    continue;
                }

                paths.push(PathDefinition {
                    activated: event,
                    dependencies: events,
                });
            }

            for &emitter in &event_node.emitters {
                let mut events = Vec::new();
                let emitter_node = &self.emitters()[emitter];
                for &responder in &emitter_node.responders {
                    let responder_node = &self.responders()[responder];
                    events.push(responder_node.event);
                }

                if events.len() < 2 {
                    continue;
                }

                parallels.push(ParallelDefinition {
                    dependency: event,
                    parallels: events,
                });
            }
        }

        NetDefinition {
            events: self.events().len(),
            paths,
            parallels,
        }
    }

    fn responder_paths(&self, responder: usize) -> Set<usize> {
        let mut events = Set::new();

        let responder_node = &self.responders()[responder];
        for &emitter in &responder_node.emitters {
            events.insert(self.emitters()[emitter].event);
        }

        events
    }

    fn remove_responder_path(&mut self, responder: usize) {
        let responder_node = &self.responders()[responder];
        for emitter in responder_node.emitters.clone() {
            self.remove_emitter(emitter);
        }
        self.remove_responder(responder);
    }

    fn remove_duplicates_before(&mut self, dependency: usize) -> bool {
        let dependency_node = &self.events()[dependency];
        let responders = dependency_node.responders.clone();
        let mut responders = responders.iter();

        while let Some(&responder) = responders.next() {
            for &other_responder in responders.clone() {
                let events = self.responder_paths(responder);
                let other_events = self.responder_paths(other_responder);

                if events.is_subset(&other_events) {
                    self.remove_responder_path(other_responder);
                } else if other_events.is_subset(&events) {
                    self.remove_responder_path(responder);
                } else {
                    continue;
                }

                return true;
            }
        }

        false
    }

    pub fn add_path(&mut self, activated: usize, dependencies: &[usize]) {
        let responder = self.add_responder(activated);
        for &dependency in dependencies {
            let emitter = self.add_emitter(dependency);
            self.connect(emitter, responder);
        }

        while self.remove_duplicates_before(activated) {}
    }

    fn emitter_paths(&self, emitter: usize) -> Set<usize> {
        let mut events = Set::new();

        let emitter_node = &self.emitters()[emitter];
        for &responder in &emitter_node.responders {
            events.insert(self.responders()[responder].event);
        }

        events
    }

    fn remove_emitter_path(&mut self, emitter: usize) {
        let emitter_node = &self.emitters()[emitter];
        for responder in emitter_node.responders.clone() {
            self.remove_responder(responder);
        }
        self.remove_emitter(emitter);
    }

    fn remove_duplicates_after(&mut self, dependency: usize) -> bool {
        let dependency_node = &self.events()[dependency];
        let emitters = dependency_node.emitters.clone();
        let mut emitters = emitters.iter();

        while let Some(&emitter) = emitters.next() {
            for &other_emitter in emitters.clone() {
                let events = self.emitter_paths(emitter);
                let other_events = self.emitter_paths(other_emitter);

                if events.is_subset(&other_events) {
                    self.remove_emitter_path(emitter);
                } else if other_events.is_subset(&events) {
                    self.remove_emitter_path(other_emitter);
                } else {
                    continue;
                }

                return true;
            }
        }

        false
    }

    fn responders_between(&self, emitter: usize, event: usize) -> Vec<usize> {
        self.emitters()[emitter]
            .responders
            .iter()
            .copied()
            .filter(|&responder| self.responders()[responder].event == event)
            .collect()
    }

    pub fn make_parallel(&mut self, dependency: usize, parallels: &[usize]) {
        let dependency_node = &self.events()[dependency];

        let mut join_paths_list = Vec::new();
        for &parallel in parallels {
            let mut join_paths = Vec::new();
            for &emitter in &dependency_node.emitters {
                let responders = self.responders_between(emitter, parallel);
                for responder in responders {
                    join_paths.push((emitter, responder));
                }
            }
            if !join_paths.is_empty() {
                join_paths_list.push(join_paths);
            }
        }

        let paths_count = join_paths_list.len();

        let mut indices = vec![0; paths_count];
        let mut running = true;
        while running {
            let emitter = self.add_emitter(dependency);

            for (&index, join_paths) in indices.iter().zip(&join_paths_list) {
                let (path_emitter, path_responder) = join_paths[index];
                let responder_duplicate = self.duplicate_responder(path_responder);

                self.disconnect(path_emitter, responder_duplicate);
                self.connect(emitter, responder_duplicate);
            }

            for i in 0.. {
                if i == paths_count {
                    running = false;
                    break;
                }

                indices[i] += 1;
                if indices[i] < join_paths_list[i].len() {
                    break;
                }

                if i == paths_count - 1 {
                    running = false;
                    break;
                }

                indices[i] = 0;
            }
        }

        while self.remove_duplicates_after(dependency) {}
    }
}
