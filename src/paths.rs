use crate::utilities::ReusableList;

use std::collections::BTreeSet as Set;

#[derive(Clone)]
pub struct EmitterNode {
    pub event: usize,
    pub responders: Set<usize>,
}

impl EmitterNode {
    fn new(event: usize) -> Self {
        Self {
            event,
            responders: Set::new(),
        }
    }
}

#[derive(Clone)]
pub struct ResponderNode {
    pub event: usize,
    pub emitters: Set<usize>,
}

impl ResponderNode {
    fn new(event: usize) -> Self {
        Self {
            event,
            emitters: Set::new(),
        }
    }
}

#[derive(Default)]
pub struct Paths {
    responders: ReusableList<ResponderNode>,
    emitters: ReusableList<EmitterNode>,
}

impl Paths {
    pub fn responders(&self) -> &[ResponderNode] {
        &self.responders[..]
    }

    pub fn emitters(&self) -> &[EmitterNode] {
        &self.emitters[..]
    }

    pub fn is_responder_of(&self, responder: usize, event: usize) -> bool {
        self.responders[responder].event == event
    }

    pub fn is_emitter_of(&self, emitter: usize, event: usize) -> bool {
        self.emitters[emitter].event == event
    }

    pub fn add_responder(&mut self, event: usize) -> usize {
        self.responders.add(ResponderNode::new(event))
    }

    pub fn duplicate_responder(&mut self, responder: usize) -> usize {
        let responder_node = &self.responders[responder];
        let emitters = responder_node.emitters.clone();
        let responder_node_clone = responder_node.clone();
        let responder_duplicate = self.responders.add(responder_node_clone);
        for emitter in emitters {
            self.emitters[emitter]
                .responders
                .insert(responder_duplicate);
        }
        responder_duplicate
    }

    pub fn remove_responder(&mut self, responder: usize) {
        let responder_node = &self.responders[responder];
        for &emitter in &responder_node.emitters {
            self.emitters[emitter].responders.remove(&responder);
        }
        self.responders.remove(responder);
    }

    pub fn add_emitter(&mut self, event: usize) -> usize {
        self.emitters.add(EmitterNode::new(event))
    }

    pub fn duplicate_emitter(&mut self, emitter: usize) -> usize {
        let emitter_node = &self.emitters[emitter];
        let responders = emitter_node.responders.clone();
        let emitter_node_clone = emitter_node.clone();
        let emitter_duplicate = self.emitters.add(emitter_node_clone);
        for responder in responders {
            self.responders[responder]
                .emitters
                .insert(emitter_duplicate);
        }
        emitter_duplicate
    }

    pub fn remove_emitter(&mut self, emitter: usize) {
        let emitter_node = &self.emitters[emitter];
        for &responder in &emitter_node.responders {
            self.responders[responder].emitters.remove(&emitter);
        }
        self.emitters.remove(emitter);
    }

    pub fn connect(&mut self, emitter: usize, responder: usize) {
        self.emitters[emitter].responders.insert(responder);
        self.responders[responder].emitters.insert(emitter);
    }

    pub fn disconnect(&mut self, emitter: usize, responder: usize) {
        self.emitters[emitter].responders.remove(&responder);
        self.responders[responder].emitters.remove(&emitter);
    }

    pub fn is_connected(&self, emitter: usize, responder: usize) -> bool {
        self.emitters[emitter].responders.contains(&responder)
    }
}
