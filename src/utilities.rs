use std::ops::{Index, IndexMut};

pub struct ReusableList<T> {
    list: Vec<T>,
    reusable: Vec<usize>,
}

impl<T> Default for ReusableList<T> {
    fn default() -> Self {
        Self {
            list: Vec::new(),
            reusable: Vec::new(),
        }
    }
}

impl<T> ReusableList<T> {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn add(&mut self, value: T) -> usize {
        if let Some(index) = self.reusable.pop() {
            self.list[index] = value;
            index
        } else {
            let index = self.list.len();
            self.list.push(value);
            index
        }
    }

    pub fn remove(&mut self, index: usize) {
        if !self.reusable.contains(&index) {
            self.reusable.push(index);
        }
    }
}

impl<T, I: std::slice::SliceIndex<[T]>> Index<I> for ReusableList<T> {
    type Output = <Vec<T> as Index<I>>::Output;

    fn index(&self, index: I) -> &Self::Output {
        self.list.index(index)
    }
}

impl<T, I: std::slice::SliceIndex<[T]>> IndexMut<I> for ReusableList<T> {
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        self.list.index_mut(index)
    }
}

impl<'a, T> IntoIterator for &'a ReusableList<T> {
    type Item = &'a T;
    type IntoIter = std::slice::Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.list.iter()
    }
}
