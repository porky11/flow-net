use crate::net::Net;

use std::io::Write;

impl Net {
    pub fn to_dot<W: Write>(&self, names: &[String], w: &mut W) {
        let _ = writeln!(w, "digraph {{");

        for (event, event_node) in self.events().iter().enumerate() {
            let label = names
                .get(event)
                .map_or_else(String::new, |name| format!(", label = {name:?}"));

            let _ = writeln!(w, "    e{event} [shape = rect{label}]");

            for &responder in &event_node.responders {
                let _ = writeln!(w, "    i{responder} [shape = point]");
                let _ = writeln!(w, "    i{responder} -> e{event}");
            }
            for &emitter in &event_node.emitters {
                let _ = writeln!(w, "    o{emitter} [shape = point]",);
                let _ = writeln!(w, "    e{event} -> o{emitter}");
            }
        }

        for (emitter, emitter_node) in self.emitters().iter().enumerate() {
            for &responder in &emitter_node.responders {
                let _ = writeln!(w, "    o{emitter} -> i{responder}");
            }
        }

        let _ = writeln!(w, "}}");
    }
}
